if (!require('os').platform == 'linux') throw new RangeError('This bot may only be ran on Linux based systems.');

const config = require('./config.json');

const Eris = require('eris');
const client = new Eris.CommandClient(config.token, {
  autoreconnect: true,
  getAllUsers: false,
  restMode: true,
  defaultImageFormat: 'png'
}, {
  defaultHelpCommand: true,
  name: 'Raspberry Pi | Utilities',
  owner: 'Dutch van der Linde#0001',
  prefix: ['@mention ', '='],
  defaultCommandOptions: {
    cooldown: 2000
  }
});

client.on('ready', () => {
  console.log(`${client.user.username} is ready in ${client.guilds.size} servers!`);
});

const fs = require('fs');

function checkTemperature() {
  const file = fs.readFileSync('/sys/class/thermal/thermal_zone0/temp');
  const temp = file / 1000;
  return temp;
}

setInterval(async () => {
  const thisTemp = checkTemperature();
  if (thisTemp >= 66) {
    client.users.get('278620217221971968').getDMChannel().then(r => {
      r.createMessage(`Raspberry Pi Temperature Warning | Temperature is currently '${thisTemp}°C, please take action.`);
    });
    if (thisTemp >= 71) {
      await client.users.get('278620217221971968').getDMChannel().then(r => {
        r.createMessage(`Raspberry Pi has reached critical temperatures of ${thisTemp}°C, shutting down to prevent damage.`);
        setTimeout(() => require('child_process').exec('sudo reboot'), 1000);
      });
    }
  }
}, 5000);

client.registerCommand('ping', async message => {
  const then = Date.now();
  const newmsg = await message.channel.createMessage('Pong!');
  const diff = Date.now() - then;
  newmsg.edit(`Pong! \`${diff}ms\``);
});

client.registerCommand('eval', async (message, args) => {
  const code = args.join(' ');
  let evaled;

  try {
    evaled = await eval(code);
    if (typeof evaled === 'object') {
      evaled = require('util').inspect(evaled, {
        depth: 0
      });
    }
  } catch (err) {
    const errorEmbed = {
      title: 'JavaScript Eval',
      color: 16711680,
      description: `\`\`\`xl\n${err}\`\`\``,
      timestamp: new Date(message.createdAt),
      footer: {
        text: client.user.username,
        icon_url: client.user.avatarURL
      }
    };
    return message.channel.createMessage({embed: errorEmbed});
  }
    
  if (typeof evaled === 'string') {
    evaled = evaled.replace(client.token, '[TOKEN]');
  }

  if (typeof evaled === 'object') {
    evaled = require('util').inspect(evaled, {depth: 0});
  }
    
  if (evaled == undefined) {
    evaled = 'undefined';
  }

  if (evaled.length > 1900) {
    evaled = 'Response too large to be sent.';
  }

  const successEmbed = {
    title: 'JavaScript Eval',
    color: 65280,
    description: `\`\`\`js\n${evaled}\`\`\``,
    timestamp: new Date(message.createdAt),
    footer: {
      text: client.user.username,
      icon_url: client.user.avatarURL
    }
  };
  return message.channel.createMessage({embed: successEmbed});
}, {
  'description': 'Evaluates JavaScript code.',
  'aliases': ['e'],
  'cooldown': 0,
  'hidden': true,
  'requirements': {
    'userIDs': ['278620217221971968']
  }
} 
);

client.registerCommand('temp', message => {
  const c = checkTemperature();
  const ac = c.toString();
  message.channel.createMessage(`The current temperature is ${ac}°C`);
});

client.registerCommand('who', async message => {
  const exec = await require('child_process').exec('who');
  message.channel.createMEssage(exec);
})


client.connect();